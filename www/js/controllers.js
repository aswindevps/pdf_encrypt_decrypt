angular.module('pdf-en.controllers', ['pdf-en.factories'])

.controller('PdfCtrl', function($scope, $cordovaFileOpener2, $http, $cordovaFile, backendCipherService, $localStorage, $ionicModal) {
    $scope.encrypt = function() {
        $http.get('http://s3.amazonaws.com/concept-owl/question_source/171/10_Functions___Inverse_trigonometric_functions.pdf?1433940938', {
            responseType: "blob"
        }).then(function(response) {
            //console.log(response);
            // url = URL.createObjectURL(response.data);
            // window.open(url);
            backendCipherService.encryptPDF(response.data, 'sensomate').then(function(data) {
                $scope.encryptData = data.json;
                //console.log(data);
            });
        }, function(error) {
            //console.log(error)
            // deferred.reject(error);
        });
    }
    $scope.decrypt = function() {
         var pdfBlob = new Blob([backendCipherService.decryptPDF($scope.encryptData, 'sensomate')], {
                type: 'application/pdf'
        });
        //console.log(pdfBlob);
        var vm = this;
       
        this.setDefaultsForPdfViewer($scope);
        // Initialize the modal view.
        $ionicModal.fromTemplateUrl('pdf-viewer.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            vm.modal = modal;
            $scope.pdfUrl = URL.createObjectURL(pdfBlob);
            //console.log($scope);
            vm.modal.show();
        });
 
    }
    $scope.setDefaultsForPdfViewer = function($scope) {
        $scope.scroll = 0;
        $scope.loading = 'loading';

        $scope.onError = function (error) {
            console.error(error);
        };

        $scope.onLoad = function () {
            $scope.loading = '';
        };

        $scope.onProgress = function (progress) {
            console.log(progress);
        };
    }

});