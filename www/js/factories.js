angular.module('pdf-en.factories', [])
    .factory('$localStorage', ['$window', function($window) {
        'use strict';
        return {
            set: function(key, value) {
                $window.localStorage[key] = value;
                return $window.localStorage[key] || defaultValue;
            },
            get: function(key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function(key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function(key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            remove: function(key) {
                delete $window.localStorage[key];
            }
        };
    }])
    .factory('backendCipherService', function($q) {
        'use strict';
        return {
            encryptPDF: function(blob, password) {
                var salt = forge.random.getBytesSync(256);
                var key = forge.pkcs5.pbkdf2(password, salt, 40, 32);
                var iv = forge.random.getBytesSync(32);
                var cipher = forge.cipher.createCipher('AES-CBC', key);

                cipher.start({
                    iv: iv
                });
                var deferred = $q.defer();

                var uint8Array = null;
                var arrayBuffer = null;
                var fileReader = new FileReader();
                fileReader.onload = function(progressEvent) {
                    arrayBuffer = this.result;
                    uint8Array = new Uint8Array(arrayBuffer);
                };
                fileReader.readAsArrayBuffer(blob);
                fileReader.onloadend = function() {
                    var inp = uint8Array;
                    //console.log(inp);
                    cipher.update(forge.util.createBuffer(inp));
                    cipher.finish();
                    var encrypted = cipher.output;
                    var data = forge.util.bytesToHex(encrypted);
                    var obj = {
                        "salt": forge.util.bytesToHex(salt),
                        "iv": forge.util.bytesToHex(iv),
                        "encrypted": data
                    };
                    deferred.resolve({
                        json: angular.toJson(obj)
                    });
                };
                return deferred.promise;
            },
            s2a: function(str) {
                var view = new Uint8Array(str.length);
                for (var i = 0, j = str.length; i < j; i++) {
                    view[i] = str.charCodeAt(i);
                }
                return view;
            },
              decryptPDF: function(json, password) {
                var obj = angular.fromJson(json);
                var key = forge.pkcs5.pbkdf2(password, forge.util.hexToBytes(obj.salt), 40, 32);
                var iv = forge.util.createBuffer();
                var data = forge.util.createBuffer();
                iv.putBytes(forge.util.hexToBytes(obj.iv));
                data.putBytes(forge.util.hexToBytes(obj.encrypted));

                var decipher = forge.cipher.createDecipher('AES-CBC', key);
                decipher.start({
                    iv: iv
                });
                decipher.update(data);

               decipher.finish();
               return this.s2a(decipher.output.getBytes());

            }
        };
    });
