angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
        $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
        $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
        console.log('Doing login', $scope.loginData);

        // Simulate a login delay. Remove this and replace with your login
        // code if using a login system
        $timeout(function() {
            $scope.closeLogin();
        }, 1000);
    };
})

.controller('PlaylistsCtrl', function($scope, $cordovaFileOpener2, $http, $cordovaFile) {
    $scope.open = function() {
        // $cordovaFileOpener2.open(
        //     '/sdcard/Download/ashwin.pdf',
        //     'application/pdf'
        // ).then(function() {
        // }, function(err) {
        // });

        $http.get('config/ashwin.pdf', {
                responseType: 'arraybuffer'
            })
            .success(function(data) {
                var file = new Blob([data], {
                    type: 'application/pdf'
                });

                var fileURL = URL.createObjectURL(file);
                console.log(fileURL);
                $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory + "/Download/", "ashwin.pdf", fileURL)
                    .then(function(successdata) {
                        console.log(successdata);
                    }, function(errorData) {
                        console.log(errorData);
                    });
            });
        // $cordovaFile.readAsBinaryString(cordova.file.externalRootDirectory + '/Download/', 'ashwin.pdf')
        //     .then(function(success) {

        //     }, function(error) {
        //         console.log(error);
        //     });

    }
})

.controller('PlaylistCtrl', function($scope, $stateParams) {});
